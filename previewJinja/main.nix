{
  inputs,
  makePythonPypiEnvironment,
  makeScript,
  projectPath,
  ...
}: let
  pythonEnvironment = makePythonPypiEnvironment {
    name = "previewJinjaEnvironment";
    sourcesYaml = ./sources.yaml;
  };
in
  makeScript {
    entrypoint = ./entrypoint.sh;
    name = "previewJinja";
    replace = {
      __argSrc__ = projectPath "/previewJinja/src";
    };
    searchPaths = {
      bin = [inputs.nixpkgs.python39];
      source = [pythonEnvironment];
    };
  }
