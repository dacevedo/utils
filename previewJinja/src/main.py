from jinja2 import Environment, FileSystemLoader
from livereload import Server
from pathlib import Path
from typing import Any, Dict
import json
import sys


def _render_template(
    output_directory: str,
    template_directory: str,
    template_name: str,
    template_context: Dict[str, Any],
) -> None:
    jinja_loader = FileSystemLoader(template_directory)
    jinja_environment = Environment(autoescape=True, loader=jinja_loader)
    output_file_path = f"{output_directory}/{template_name}"

    def _render() -> None:
        template = jinja_environment.get_template(template_name)

        with open(output_file_path, "w") as output:
            output.write(template.render(template_context))

    return _render


def _start_server(
    output_path: Path,
    template_path: Path,
    template_context: Dict[str, Any],
) -> None:
    server = Server()
    output_directory = str(output_path)
    template_directory = str(template_path.parent)
    template_name = template_path.name
    render = _render_template(
        output_directory,
        template_directory,
        template_name,
        template_context,
    )

    render()
    server.watch(filepath=template_directory, func=render)
    server.serve(default_filename=template_name, root=output_directory)


if __name__ == "__main__":
    output_path = Path(sys.argv[1])
    template_path = Path(sys.argv[2])
    template_context = json.loads(sys.argv[3])

    _start_server(output_path, template_path, template_context)
