{fetchNixpkgs, ...}: {
  extendingMakesDirs = ["/"];
  inputs = {
    nixpkgs = fetchNixpkgs {
      rev = "f0316a346762abc8042cebad541898ba17476391";
      sha256 = "sha256-KRJfqdthSSVtY6D4qZNGYk8MZSMWYFqk6QcCNqY6WOE=";
    };
  };
}